﻿using Microsoft.AspNetCore.Components;
using BlazinMinecraft.Components;
using BlazinMinecraft.Models;
using BlazinMinecraft.Services;

namespace BlazinMinecraft.Pages
{
    public partial class Index
    {
        private Cake CakeItem = new()
        {
            Id = 1,
            Name = "Black Forest",
            Cost = 50
        };

        [Inject]
        public IDataService DataService { get; set; }

        public List<Item> Items { get; set; } = new List<Item>();

        private List<CraftingRecipe> Recipes { get; set; } = new List<CraftingRecipe>();

        protected override async Task OnAfterRenderAsync(bool firstRender)
        {
            base.OnAfterRenderAsync(firstRender);

            if (!firstRender)
            {
                return;
            }

            Items = await DataService.List(0, await DataService.Count());
            Recipes = await DataService.GetRecipes();

            StateHasChanged();
        }
    }
}
