﻿using BlazinMinecraft.Models;

namespace BlazinMinecraft.Components
{
    public class CraftingAction
    {
        public string Action { get; set; }
        public int Index { get; set; }
        public Item Item { get; set; }
    }
}
