# BlazinMinecraft

This (unfinished) project was part of a Blazor course taught by Mr. Julien Rioboulet at Clermont-Ferrand's IUT.
The assignment was to create a minecraft inventory with crafting options.

*Note for Mr. Riboulet:*
Due to poor time management and other issues out of my control, as detailed in my email, I was unable to complete the project.
Again, sorry for handing in work of such poor quality.

## Starting up

**Before you try starting up, make sure you are running a Windows computer (Windows 10 or 11) with Visual Studio 2022 installed.**

### Cloning the repository

1. Boot up Visual Studio 2022
2. In the opening menu, select **Clone a repository**.
3. Enter `https://codefirst.iut.uca.fr/git/elliott.le_guehennec/BlazinMinecraft.git` as the path to the repository.
4. Enter your IUT Gitea identifiers in the pop-up window. 
    - *If you have defined them as your global identifiers, this step will not be required.*

*Alternatively you can clone the repository using git bash or another git client.*

### Launching the app

1. Open solution BlazinMinecraft with Visual Studio 2022.
    - *If you cloned the repository following the steps above, you already have the solution open on Visual Studio. This step can be skipped.*
    - *File is located at* `BlazinMinecraft/Sources/BlazinMinecraft.sln`
2. Make sure Solution explorer is visible.
    - *It should be visible by default. If not, press* `Ctrl`+`Alt`+`L`
3. On solution explorer, select the solution file (at the very top) and press `Alt`+`Enter` to open the file Properties.
    - *This can also be done through a right click on the solution file*
4. Open `Common Properties`, then `Startup Project`
    - *May be selected by default*
5. Select `Multiple startup projects`
6. Make sure projects `BlazinMinecraft` and `Minecraft.Crafting.Api` have their Action set to Start.
7. Press `OK` or `Apply` to save your changes
8. Press the green play button at the top of the screen, or the `F5` key, to start the app.
9. Upon your first startup you may see a series of prompt windows by Visual Studio asking for various authorizations. **Accept all of them.**
10. Wait a few seconds for the project to finish booting.